package nougat.quizappnougat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import utils.Constants;

public class ListQuizActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listquiz);

        final Intent intent = getIntent();
        if (null != intent) {
            final Bundle extras = intent.getExtras();
            if ((null != extras) && (extras.containsKey(Constants.Login.LOGIN))) {
                // Retrieve the login
                final String login = extras.getString(Constants.Login.LOGIN);

                // Set as ActionBar subtitle
                getSupportActionBar().setSubtitle(login);
            }
        }
    }
}
