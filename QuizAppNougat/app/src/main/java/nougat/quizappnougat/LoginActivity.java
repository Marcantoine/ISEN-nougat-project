package nougat.quizappnougat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import utils.Constants;

/**
 * Created by Vivien on 01/03/2017.
 */

public class LoginActivity extends Activity implements View.OnClickListener{

    private EditText mLoginEditText;

    private EditText mPasswordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLoginEditText = (EditText) findViewById(R.id.loginEditText);
        mPasswordEditText = (EditText) findViewById(R.id.passwordEditText);

        findViewById(R.id.loginButton).setOnClickListener(this);
    }

    private Intent getHomeActivityIntent(String userName){
        final Intent intent = new Intent(this, ListQuizActivity.class);
        final Bundle extras = new Bundle();
        extras.putString(Constants.Login.LOGIN, userName);
        intent.putExtras(extras);
        return intent;
    }

    @Override
    public void onClick(View v) {
        if (TextUtils.isEmpty(mLoginEditText.getText())){
            // Display a Toast to the user
            Toast.makeText(this, R.string.error_no_login, Toast.LENGTH_LONG).show();
            return;
        }

        // Check if a password is set
        if (TextUtils.isEmpty(mPasswordEditText.getText())){
            // Display a Toast to the user
            Toast.makeText(this, R.string.error_no_password, Toast.LENGTH_LONG).show();
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
        }).start();

        final Intent intent = getHomeActivityIntent(mLoginEditText.getText().toString());
        startActivity(intent);
    }
}
